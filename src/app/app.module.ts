import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { RouterModule } from "@angular/router";
import { AdminComponent } from "./admin/admin.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthGuard } from "./auth.guard";
import { AuthService } from "./auth.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatCardModule } from "@angular/material/card";
import { YoutubePlayerModule } from "ng2-youtube-player";
import { CustomMaterialModule } from "./core/material.module";
import { MatTabsModule } from "@angular/material/tabs";
import { VideosComponent } from "./videos/videos.component";
import { HttpClientModule } from "@angular/common/http";
import { MatAutocompleteModule } from "@angular/material";
import { MatSidenavModule } from "@angular/material";
import { MatGridListModule } from "@angular/material";
import { MatChipsModule } from "@angular/material/chips";
import { MatDividerModule } from "@angular/material/divider";
import { MatListModule } from "@angular/material/list";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatProgressSpinnerModule } from "@angular/material";
import { OrderModule } from "ngx-order-pipe";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AdminComponent,
    VideosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    ReactiveFormsModule,
    CustomMaterialModule,
    YoutubePlayerModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatDividerModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatSidenavModule,
    MatGridListModule,
    MatChipsModule,
    OrderModule,
    RouterModule.forRoot([
      {
        path: "login",
        component: LoginComponent
      },
      {
        path: "",
        component: HomeComponent
      },
      {
        path: "admin",
        component: AdminComponent,
        canActivate: [AuthGuard]
      }
    ])
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}

import { TestBed } from '@angular/core/testing';

import { DivertissementService } from './divertissement.service';

describe('DivertissementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DivertissementService = TestBed.get(DivertissementService);
    expect(service).toBeTruthy();
  });
});

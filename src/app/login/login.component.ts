import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";
import * as Parse from "parse";
import { Router } from "@angular/router";

Parse.initialize("placeId", "");
(Parse as any).serverURL = "http://localhost:1337/api";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  etat: boolean = false;

  constructor(private router: Router, private Auth: AuthService) {}

  ngOnInit() {}

  login(): void {
    this.router.navigate(["admin"]);
    Parse.User.logIn(this.username, this.password).then(
      function(user) {
        //this.router.navigate(['admin']);
        console.log(
          "User created successful with name: " +
            user.get("username") +
            " and email: " +
            user.get("email")
        );

        //this.etat=true;
      },
      err => {
        console.log(err);
      }
    );
    setTimeout(function() {
      //console.log(this.videos)
    }, 500);

    if ((this.etat = true)) {
      this.Auth.setLoggedIn(true);
      this.router.navigate(["admin"]);
    }
  }
}

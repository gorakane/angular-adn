import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { DivertissementService } from "../divertissement.service";
import { Router } from "@angular/router";
import * as Parse from "parse";

Parse.initialize("placeId", "");
(Parse as any).serverURL = "http://localhost:1337/api";

@Component({
  selector: "app-videos",
  templateUrl: "./videos.component.html",
  styleUrls: ["./videos.component.css"]
})
export class VideosComponent implements OnInit {
  constructor(
    private divertissementService: DivertissementService,
    private routes: Router
  ) {}
  public videos: any;
  people: any[];
  player: YT.Player;
  commentaires;
  numberofComments;
  inputComment;
  id_video;

  @Input()
  id: String;

  @Input()
  videoId: String;

  ngOnInit() {
    var req = Parse.Object.extend("Videos");
    var query = new Parse.Query(req);
    query.find().then(
      data => {
        this.videos = JSON.parse(JSON.stringify(data));
      },
      err => {
        console.log(err);
      }
    );
    setTimeout(function() {}, 500);
  }

  onDisplayCommentaire(value) {
    this.listOfComments(value.objectId);
    var req = Parse.Object.extend("Videos");
    var query2 = new Parse.Query(req);
    this.id_video = value.objectId;

    query2.equalTo("objectId", value.objectId);
    query2.find().then(
      data => {
        this.id = JSON.parse(JSON.stringify(data))[0].video_link;

        this.videoId = JSON.parse(JSON.stringify(data))[0].video_link;
      },
      err => {
        console.log(err);
      }
    );
    setTimeout(function() {
      //console.log(this.videos)
    }, 55);
  }

  savePlayer(player) {
    this.player = player;
    //this.id="qDuKsiwS5xw";
    console.log("player instance", player.getVideo);
  }

  onStateChange(event) {
    console.log("player state", event);
  }

  listOfComments(val) {
    var comments = Parse.Object.extend("Commentaires");
    var query = new Parse.Query(comments);
    query.equalTo("videos_id", val);
    query
      .find()

      .then(
        data => {
          this.commentaires = JSON.parse(JSON.stringify(data));
        },
        err => {
          console.log(err);
        }
      );
    setTimeout(function() {
      //console.log(this.videos)
    }, 500);

    var query2 = new Parse.Query(comments);

    query2.equalTo("videos_id", val);
    query2.count().then(
      num => {
        this.numberofComments = num;
      },
      err => {
        console.log(err);
      }
    );
    setTimeout(function() {
      //console.log(this.videos)
    }, 500);
  }

  doComment(c) {
    console.log(c.value.myText);
    var comments = Parse.Object.extend("Commentaires");
    var query = new comments();
    query.set("libelle", c.value.myText);
    query.set("videos_id", this.id_video);
    query.save().then(
      data => {
        console.log(data.id);
        this.listOfComments(this.id_video);
        this.inputComment = "";
      },
      err => {
        console.log(err);
      }
    );
    setTimeout(function() {
      //console.log(this.videos)
    }, 500);
  }
}

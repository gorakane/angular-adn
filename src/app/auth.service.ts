import { Injectable } from '@angular/core';
import * as Parse from 'parse';
import { Router } from '@angular/router';

interface myData{
  success:boolean;
  message:string
}

Parse.initialize('placeId', ''); 
(Parse as any).serverURL = 'http://localhost:1337/api';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private router : Router;
 
  LoggedInStatus=false;
  constructor() { }

  setLoggedIn(value:boolean){
    this.LoggedInStatus=value
  }
 
  get isLoggedIn(){
    return this.LoggedInStatus
  }

  redirect(){
    this.router.navigate(['']);
  }
}